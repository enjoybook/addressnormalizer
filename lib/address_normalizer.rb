require 'address_normalizer/configuration'
require 'address_normalizer/processor'

module AddressNormalizer

  class << self

    attr_accessor :configuration

    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield(configuration)
    end

    def normalize(source)
      Processor.new.normalize source
    end

    def parse_status(data)
      Processor.new.parse_status data
    end

  end

end
