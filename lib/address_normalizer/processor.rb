require 'address_normalizer/pochta_russia'
require 'address_normalizer/dadata'

module AddressNormalizer

  class Processor

    ERROR_TIPS = {
      foreign: 'Иностранный адрес',
      impossible_to_normalize: 'Невозможно разобрать адрес!',
      impossible_to_parse_status: 'Невозможно определить статус!'
    }.freeze

    def initialize
      @dadata = Dadata.new
      @pochta_russia = PochtaRussia.new
    end

    def normalize(source)
      response = @pochta_russia.normalize source
      result = success?(response) ? process(response) : normalize_with_dadata(source)

      result.except('validation-code', 'quality-code')
    end

    def parse_status(data)
      @pochta_russia.parse_status(data) || @dadata.parse_status(data) || impossible_to_parse_status
    end

    private

    def process(response)
      body = response[:body][0]

      return body if @pochta_russia.can_use? body
      return foreign_address if @pochta_russia.foreign? body

      @pochta_russia.unclean?(body) ? try_to_repair(body['source']) : impossible_to_normalize
    end

    def try_to_repair(source)
      result = normalize_with_dadata source
      @dadata.can_use?(result) ? result : try_to_repair_with_pochta_russia(result)
    end

    def try_to_repair_with_pochta_russia(dadata_result)
      parsed_address = @pochta_russia.normalize dadata_result[:body][0]['source']
      repaired?(parsed_address) ? parsed_address : dadata_result
    end

    def normalize_with_dadata(source)
      result = @dadata.normalize source
      return result if final?(result)
      @dadata.repair result
    end

    def final?(result)
      !success?(result) || @dadata.can_use?(result)
    end

    def repaired?(parsed_address)
      success?(parsed_address) && @pochta_russia.can_use?(parsed_address)
    end

    def success?(result)
      result[:status] == :success
    end

    def foreign_address
      { status: 'incorrect', error_tip: ERROR_TIPS[:foreign] }
    end

    def impossible_to_normalize
      { status: 'incorrect', error_tip: ERROR_TIPS[:impossible_to_normalize] }
    end

    def impossible_to_parse_status
      { status: 'incorrect', error_tip: ERROR_TIPS[:impossible_to_parse_status] }
    end

  end

end
