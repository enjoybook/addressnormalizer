module AddressNormalizer

  class Configuration

    attr_accessor :dadata_api_key, :dadata_secret_key
    attr_accessor :pochta_russia_access_token, :pochta_russia_login_password

  end

end
