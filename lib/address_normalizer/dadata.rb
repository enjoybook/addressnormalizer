require 'address_normalizer/sender'
require 'active_support'

module AddressNormalizer

  class Dadata

    API_URL = 'https://dadata.ru/api/v2/clean/address'.freeze

    WHITE_LIST_PARAMS = [
      'source',
      'postal_code',
      'country',
      'region',
      'region_type',
      'area',
      'area_type',
      'city',
      'city_type',
      'city_area',
      'city_district',
      'city_district_type',
      'settlement',
      'settlement_type',
      'street',
      'street_type',
      'house',
      'flat',
      'timezone'
    ].freeze

    def initialize
      @sender = AddressNormalizer::Sender.new

      @request_headers = {
        'X-Secret' => AddressNormalizer.configuration.dadata_secret_key.to_s,
        'Content-Type' => 'application/json',
        'Authorization' => "Token #{AddressNormalizer.configuration.dadata_api_key}"
      }
    end

    def normalize(source)
      response = @sender.request API_URL, @request_headers, [source]
      process_response response
    end

    def parse_status(params)
      return { status: 'incorrect' } unless params
      return unless can_parse_status?(params)

      if can_use? params
        { status: get_status(params), error_tip: nil }
      else
        { status: 'incorrect', error_tip: get_error_tip(params) }
      end
    end

    def clean(data)
      source = data['source']
      unparsed_parts = data['normalized_address']['unparsed_parts']

      if unparsed_parts
        unparsed_parts.split(', ').each { |part| source = source.gsub /#{part}/i, '' }
      end

      source
    end

    def repair(data)
      return normalize(clean(data)) if unclean? data
      data
    end

    def can_use?(data)
      # if qc param is equal to 0, then the postcode was defined correctly.
      data['qc'].to_i.zero?
    end

    private

    def process_response(response)
      if response[:status] == :success
        grab_params response[:body][0]
      else
        # Dadata responds about error in different ways
        { status: 'incorrect', error_tip: response[:body]['detail'] || response[:body]['details'].first }
      end
    end

    def grab_params(parsed_address)
      params = parsed_address.slice *WHITE_LIST_PARAMS
      params['normalized_address'] = parsed_address
      params['city'] = get_city params
      params['postcode'] = params['postal_code']
      params['error_tip'] = nil
      params.except 'postal_code'
    end

    def unclean?(data)
      data['normalized_address']['unparsed_parts']
    end

    def get_status(body)
      body['qc'].to_i.zero? ? 'correct' : 'incorrect'
    end

    def get_error_tip(body)
      check_qc_complete body['qc_complete']
    end

    def get_city(params)
      if params['city'].nil? && params['settlement'].nil? && params['region_type'] == 'г'
        params['region']
      else
        params['city']
      end
    end

    def can_parse_status?(params)
      params['qc']
    end

    def check_qc_complete(qc_complete)
      case qc_complete.to_i
      when 1 then 'Нет региона'
      when 2 then 'Нет города'
      when 3 then 'Нет улицы'
      when 4 then 'Нет дома'
      when 5 then 'Нет квартиры'
      when 6 then 'Адрес неполный'
      when 7 then 'Иностранный адрес'
      when 8 then 'Подходит для писем, но не для курьерской доставки'
      when 9 then 'Проверьте, правильно ли разобран исходный адрес'
      when 10 then 'Дома нет в ФИАС'
      end
    end

  end

end
