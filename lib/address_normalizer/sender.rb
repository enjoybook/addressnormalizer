require 'json'
require 'faraday'

module AddressNormalizer

  class Sender

    def request(url, headers, data)
      conn = Faraday.new url

      response = conn.post do |req|
        req.body = data.to_json
        req.headers = headers
      end

      status = response.success? ? :success : :error

      { status: status, body: parse(response.body) }
    end

    private

    def parse(json)
      JSON.parse json
    rescue JSON::ParserError => e
      json
    end

  end

end
