require 'address_normalizer/sender'
require 'address_normalizer/dadata'

module AddressNormalizer

  class PochtaRussia

    API_URL = 'https://otpravka-api.pochta.ru/1.0/clean/address'.freeze

    QUALITY_CODES = ['GOOD', 'POSTAL_BOX', 'ON_DEMAND', 'UNDEF_05'].freeze
    VALIDATION_CODES = ['VALIDATED', 'OVERRIDDEN', 'CONFIRMED_MANUALLY'].freeze

    WHITE_LIST_PARAMS = [
      'area',
      'house',
      'original-address',
      'region',
      'index',
      'room',
      'street',
      'validation-code',
      'quality-code'
    ].freeze

    def initialize
      @sender = AddressNormalizer::Sender.new

      @request_headers = {
        'Content-Type' => 'application/json;charset=UTF-8',
        'Authorization' => "AccessToken #{AddressNormalizer.configuration.pochta_russia_access_token}",
        'X-User-Authorization' => "Basic #{AddressNormalizer.configuration.pochta_russia_login_password}"
      }
    end

    def normalize(source)
      response = @sender.request API_URL, @request_headers, prepare_address(source)
      process_response response
    end

    def parse_status(params)
      return { status: 'incorrect' } unless params
      return unless can_parse_status?(params)

      if can_use? params
        { status: 'correct', error_tip: nil }
      else
        { status: 'incorrect', error_tip: get_error_tip(params) }
      end
    end

    def can_use?(parsed_address)
      qualified?(parsed_address) && valid?(parsed_address)
    end

    def unclean?(parsed_address)
      parsed_address['validation-code'] == 'NOT_VALIDATED_HAS_UNPARSED_PARTS'
    end

    def foreign?(parsed_address)
      parsed_address['validation-code'] == 'NOT_VALIDATED_FOREIGN'
    end

    private

    def process_response(response)
      if success?(response)
        reconstruct response
      else
        { status: 'incorrect', error_tip: response[:body] }
      end
    end

    def prepare_address(source)
      [{ 'id' => 1, 'original-address' => source }]
    end

    def valid?(parsed_address)
      VALIDATION_CODES.include? parsed_address['validation-code']
    end

    def qualified?(parsed_address)
      QUALITY_CODES.include? parsed_address['quality-code']
    end

    def can_parse_status?(params)
      params['quality-code'] && params['validation-code']
    end

    def get_error_tip(params)
      "Код качества: #{params['quality-code']}, Код проверки: #{params['validation-code']}"
    end

    def reconstruct(response)
      parsed_address = response[:body][0]
      params = parsed_address.clone

      response[:body][0] = reconstruct_params params, parsed_address

      response
    end

    # TODO: refact
    def reconstruct_params(params, parsed_address)
      params = params.slice *WHITE_LIST_PARAMS

      additional_data = grab_dadata params['original-address']
      params = params.merge additional_data

      params['source'] = params['original-address']
      params['postcode'] = params['index']
      params['flat'] = params['room']
      params['normalized_address'] = parsed_address

      params['error_tip'] = nil

      params.except 'original-address', 'index', 'room'
    end

    def grab_dadata(source)
      parsed_address = Dadata.new.normalize source
      parsed_address.slice *['country', 'timezone', 'region_type', 'area_type', 'city_type', 'city_district_type', 'settlement_type', 'street_type']
    end

    def success?(response)
      response[:status] == :success
    end

  end

end
