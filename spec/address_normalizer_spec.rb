require 'spec_helper'

RSpec.describe AddressNormalizer do
  let(:source) { 'Санкт-Петербург' }

  it 'has a version number' do
    expect(AddressNormalizer::VERSION).not_to be nil
  end

  describe '#configure' do
    subject { described_class }

    before do
      AddressNormalizer.configure do |config|
        config.dadata_api_key = 'dadata_api_key_test'
        config.dadata_secret_key = 'dadata_secret_key_test'
      end
    end

    it 'sets parameters' do
      expect(subject.configuration.dadata_api_key).to eq('dadata_api_key_test')
      expect(subject.configuration.dadata_secret_key).to eq('dadata_secret_key_test')
    end
  end

  describe '#normalize' do
    it 'returns normalized address' do
      normalized_address = { source: source }

      processor_double = instance_double AddressNormalizer::Processor, normalize: normalized_address
      allow(AddressNormalizer::Processor).to receive(:new).and_return processor_double

      expect(AddressNormalizer.normalize(source)).to eq(normalized_address)
    end
  end

  describe '#parse_status' do
    it 'returns status of normalized address' do
      data = { source: source }
      success_status = { status: 'success', error_tip: nil }
      processor_double = instance_double AddressNormalizer::Processor, parse_status: success_status
      allow(AddressNormalizer::Processor).to receive(:new).and_return processor_double

      expect(AddressNormalizer.parse_status(data)).to eq(success_status)
    end
  end
end
