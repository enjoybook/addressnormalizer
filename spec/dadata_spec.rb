require 'spec_helper'

RSpec.describe AddressNormalizer::Dadata do
  subject { dadata.normalize source_address }
  let(:dadata) { described_class.new }

  let(:source_address) { 'Санкт-Петербург, ул.Туристская, д.28 к.1, кв.117' }
  let(:body) { { 'source'=>source_address, 'country'=>'Россия', 'flat'=>'117', 'area'=>nil, 'city'=>nil, 'settlement'=>nil, 'postal_code'=>'197082', 'region_type'=>'г', 'region'=>'Санкт-Петербург','street'=>'Туристская', 'house'=>'28', 'timezone'=>'UTC+3', 'qc_complete'=>0, 'qc'=>0, 'qc_house'=>1, 'normalized_address'=>{ 'unparsed_parts'=>nil } } }
  let(:correct_status) { 'correct' }
  let(:incorrect_status) { 'incorrect' }

  describe '#normalize' do
    context 'when the response has the success status' do
      context 'and the response data has the accuracy postcode' do
        let(:sender_double) { instance_double AddressNormalizer::Sender }

        before do
          AddressNormalizer.configure do |config|
            config.dadata_api_key = 'c231b693505842b86c9eb8a0718c5e6208566c7c'
            config.dadata_secret_key = '4440b9f890b87e9c5402573239934ca9fdaa4883'
          end

          api_url = 'https://dadata.ru/api/v2/clean/address'
          request_headers = {
            'X-Secret' => AddressNormalizer.configuration.dadata_secret_key.to_s,
            'Content-Type' => 'application/json',
            'Authorization' => "Token #{AddressNormalizer.configuration.dadata_api_key}"
          }

          allow(AddressNormalizer::Sender).to receive(:new).and_return sender_double
          allow(sender_double).to receive(:request).with(api_url, request_headers, [source_address]).and_return(status: :success, body: [body])
        end

        it { is_expected.to include('source'     => source_address) }
        it { is_expected.to include('country'    => body['country']) }
        it { is_expected.to include('region'     => body['region']) }
        it { is_expected.to include('area'       => nil) }
        it { is_expected.to include('city'       => body['region']) }
        it { is_expected.to include('settlement' => body['settlement']) }
        it { is_expected.to include('street'     => body['street']) }
        it { is_expected.to include('house'      => body['house']) }
        it { is_expected.to include('flat'       => body['flat']) }
        it { is_expected.to include('postcode'   => body['postal_code']) }
        it { is_expected.to include('timezone'   => 'UTC+3') }
        it { is_expected.to include('normalized_address' => body) }
      end
    end

    # TODO: spec for details in the body... the second case
    context 'when the response has the error status' do
      let(:err_response) { { status: :err, body: { 'detail'=>'Zero balance!' } } }
      let(:sender_double) { instance_double AddressNormalizer::Sender, request: err_response }

      before do
        allow(AddressNormalizer::Sender).to receive(:new).and_return sender_double
      end

      it { is_expected.to include(status: incorrect_status) }
      it { is_expected.to include(error_tip: err_response[:body]['detail']) }
    end
  end

  describe '#parse_status' do
    let(:inaccuracy_postcode_message) { 'Индекс указан не с точностью до дома! Укажите точный индекс!' }

    context 'if there is no passed params' do
      it 'returns incorrect status' do
        expect(dadata.parse_status(nil)).to eq(status: incorrect_status)
      end
    end

    context 'if status can not be parsed' do
      it 'returns nothing' do
        expect(dadata.parse_status('pochta_russia_params_not_dadata'=>true)).to eq(nil)
      end
    end

    context 'if normalized address can be used' do
      it 'returns correct status with empty tip' do
        expect(dadata.parse_status('qc'=>0, 'qc_complete'=>10)).to eq(status: correct_status, error_tip: nil)
      end
    end

    context 'if normalized address can not be used' do
      it 'returns incorrect status with error tip' do
        expect(dadata.parse_status('qc'=>2, 'qc_complete'=>9)).to eq(status: incorrect_status, error_tip: 'Проверьте, правильно ли разобран исходный адрес')
      end
    end
  end

  describe '#clean' do
    context 'if normalized address is unclean' do
      it 'returns cleaned address' do
        unclean_body_with_unparsed_parts = { 'source'=>'Санкт-Петербург, ул.Туристская, д.28 к.1, кв.117 ЭТАЖ 3, офис 100', 'country'=>'Россия', 'region'=>'Санкт-Петербург', 'region_type'=>'г', 'area'=>nil, 'area_type'=>nil, 'city'=>'Санкт-Петербург', 'city_type'=>nil, 'city_area'=>nil, 'city_district'=>'Приморский', 'city_district_type'=>'р-н', 'settlement'=>nil, 'settlement_type'=>nil, 'street'=>'Туристская', 'street_type'=>'ул', 'house'=>'28', 'flat'=>'117', 'timezone'=>'UTC+3', 'normalized_address'=>{ 'source'=>'Санкт-Петербург, ул.Туристская, д.28 к.1, кв.117 ЭТАЖ 3, офис 100', 'result'=>'г Санкт-Петербург, ул Туристская, д 28 к 1, кв 117', 'postal_code'=>'197082', 'country'=>'Россия', 'region_fias_id'=>'c2deb16a-0330-4f05-821f-1d09c93331e6', 'region_kladr_id'=>'7800000000000', 'region_with_type'=>'г Санкт-Петербург', 'region_type'=>'г', 'region_type_full'=>'город', 'region'=>'Санкт-Петербург', 'area_fias_id'=>nil, 'area_kladr_id'=>nil, 'area_with_type'=>nil, 'area_type'=>nil, 'area_type_full'=>nil, 'area'=>nil, 'city_fias_id'=>nil, 'city_kladr_id'=>nil, 'city_with_type'=>nil, 'city_type'=>nil, 'city_type_full'=>nil, 'city'=>nil, 'city_area'=>nil, 'city_district_fias_id'=>nil, 'city_district_kladr_id'=>nil, 'city_district_with_type'=>'р-н Приморский', 'city_district_type'=>'р-н', 'city_district_type_full'=>'район', 'city_district'=>'Приморский', 'settlement_fias_id'=>nil, 'settlement_kladr_id'=>nil, 'settlement_with_type'=>nil, 'settlement_type'=>nil, 'settlement_type_full'=>nil, 'settlement'=>nil, 'street_fias_id'=>'e795fd39-846a-4d83-a715-eb9a07e43581', 'street_kladr_id'=>'78000000000142300', 'street_with_type'=>'ул Туристская', 'street_type'=>'ул', 'street_type_full'=>'улица', 'street'=>'Туристская', 'house_fias_id'=>'356c6af6-57eb-4678-8752-8536cac8e87e', 'house_kladr_id'=>'7800000000014230058', 'house_type'=>'д', 'house_type_full'=>'дом', 'house'=>'28', 'block_type'=>'к', 'block_type_full'=>'корпус', 'block'=>'1', 'flat_type'=>'кв', 'flat_type_full'=>'квартира', 'flat'=>'117', 'flat_area'=>'31.5', 'square_meter_price'=>'124830', 'flat_price'=>'3932145', 'postal_box'=>nil, 'fias_id'=>'356c6af6-57eb-4678-8752-8536cac8e87e', 'fias_level'=>'8', 'kladr_id'=>'7800000000014230058', 'capital_marker'=>'0', 'okato'=>'40270562000', 'oktmo'=>'40322000', 'tax_office'=>'7814', 'tax_office_legal'=>'7814', 'timezone'=>'UTC+3', 'geo_lat'=>'60.0032294', 'geo_lon'=>'30.2093667', 'beltway_hit'=>'IN_KAD', 'beltway_distance'=>nil, 'qc_geo'=>0, 'qc_complete'=>9, 'qc_house'=>2, 'qc'=>1, 'unparsed_parts'=>'ЭТАЖ, 3, ОФИС, 100', 'metro'=>[{ 'distance'=>4.5, 'line'=>'Фрунзенско-Приморская', 'name'=>'Крестовский остров' }, { 'distance'=>3.0, 'line'=>'Фрунзенско-Приморская', 'name'=>'Старая деревня' }, { 'distance'=>2.8, 'line'=>'Фрунзенско-Приморская', 'name'=>'Комендантский проспект' }] }, 'postcode'=>'197082' }

        expect(dadata.clean(unclean_body_with_unparsed_parts)).to eq('Санкт-Петербург, ул.Туристская, д.28 к.1, кв.117  ,  ')
      end
    end

    context 'if normalized address is not unclean' do
      it 'returns source as it is' do
        expect(dadata.clean(body)).to eq(source_address)
      end
    end
  end

  describe '#repair' do
    context 'if normalized address could be repaired' do
      let(:sender_double) { instance_double AddressNormalizer::Sender, request: { status: :success, body: [{ 'source'=>'was normalized successfully!', 'city'=>nil, 'postcode'=>nil }] } }

      before do
        allow(AddressNormalizer::Sender).to receive(:new).and_return sender_double
      end

      it 'returns repaired address' do
        normalized_address_that_could_be_repaired = { 'source' => 'Санкт-Петербург этаж 2', 'normalized_address' => { 'unparsed_parts' => 'ЭТАЖ' } }

        expect(dadata.repair(normalized_address_that_could_be_repaired)).to eq('source'=>'was normalized successfully!', 'city'=>nil, 'postcode'=>nil, 'error_tip'=>nil, 'normalized_address'=>{ 'source'=>'was normalized successfully!', 'city'=>nil, 'postcode'=>nil })
      end
    end

    context 'if normalized address could not be repaired' do
      it 'returns normalized address as it is' do
        normalized_address_that_could_not_be_repaired = { 'test_field' => 'normalized_address_that_could_not_be_repaied', 'normalized_address' => { 'unparsed_parts' => nil } }

        expect(dadata.repair(normalized_address_that_could_not_be_repaired)).to eq normalized_address_that_could_not_be_repaired
      end
    end
  end
end
