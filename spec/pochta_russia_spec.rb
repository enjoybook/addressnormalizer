require 'spec_helper'

RSpec.describe AddressNormalizer::PochtaRussia do
  let(:model) { described_class.new }

  let(:address) { 'Саратов, ул.Электронная, д. 10б, к. 24' }
  let(:prepared_address) { [{ 'id' => 1, 'original-address' => address }] }
  let(:normalized_address) { {'house'=>'10', 'region'=>'обл Саратовская', 'street'=>'ул Электронная', 'validation-code'=>'VALIDATED', 'quality-code'=>'GOOD', 'country'=>'Россия', 'timezone'=>'UTC+4', 'region_type'=>'обл', 'area_type'=>nil, 'city_type'=>'г', 'city_district_type'=>'р-н', 'settlement_type'=>nil, 'street_type'=>'ул', 'source'=>'Саратов, ул.Электронная, д. 10б, к. 24', 'postcode'=>'410064', 'flat'=>'24', 'error_tip'=>nil, 'normalized_address'=>{'address-type'=>'DEFAULT', 'house'=>'10', 'id'=>'1', 'index'=>'410064', 'letter'=>'Б', 'original-address'=>'Саратов, ул.Электронная, д. 10б, к. 24', 'place'=>'г Саратов', 'quality-code'=>'GOOD', 'region'=>'обл Саратовская', 'room'=>'24', 'street'=>'ул Электронная', 'validation-code'=>'VALIDATED'}} }
  # let(:error_body) { {'timestamp'=>'2017-10-25T15:01:30', 'status'=>400, 'error'=>'Bad Request', 'exception'=>'org.springframework.http.converter.HttpMessageNotReadableException', 'message'=>'Bad Request', 'path'=>'/1.0/clean/address'} }

  describe '#normalize' do
    let(:sender_double) { instance_double AddressNormalizer::Sender }
    let(:response) { { status: :success, body: [{"address-type"=>"DEFAULT", "house"=>"10", "id"=>"1", "index"=>"410064", "letter"=>"Б", "original-address"=>"Саратов, ул.Электронная, д. 10б, к. 24", "place"=>"г Саратов", "quality-code"=>"GOOD", "region"=>"обл Саратовская", "room"=>"24", "street"=>"ул Электронная", "validation-code"=>"VALIDATED"}] }}

    before do
      AddressNormalizer.configure do |config|
        config.pochta_russia_access_token = 'nc2NyGnj0Cu7qZ1dtxQBgimuSyoAmnJ8'
        config.pochta_russia_login_password = 'ZW5qb3lib29rLnJ1QGdtYWlsLmNvbTpQcm9zdG9wb2NodGE1Nm15bmFtZQ=='
      end

      api_url = 'https://otpravka-api.pochta.ru/1.0/clean/address'
      request_headers = {
        'Content-Type' => 'application/json;charset=UTF-8',
        'Authorization' => "AccessToken #{AddressNormalizer.configuration.pochta_russia_access_token}",
        'X-User-Authorization' => "Basic #{AddressNormalizer.configuration.pochta_russia_login_password}"
      }

      allow(AddressNormalizer::Sender).to receive(:new).and_return(sender_double)
      allow(sender_double).to receive(:request).with(api_url, request_headers, prepared_address).and_return(response)

      dadata_double = instance_double AddressNormalizer::Dadata
      allow(AddressNormalizer::Dadata).to receive(:new).and_return dadata_double

      allow(dadata_double).to receive(:normalize).with(address).and_return ({'source'=>'Саратов, ул.Электронная, д. 10б, к. 24', 'country'=>'Россия', 'region'=>'Саратовская', 'region_type'=>'обл', 'area'=>nil, 'area_type'=>nil, 'city'=>'Саратов', 'city_type'=>'г', 'city_area'=>nil, 'city_district'=>'Ленинский', 'city_district_type'=>'р-н', 'settlement'=>nil, 'settlement_type'=>nil, 'street'=>'Электронная', 'street_type'=>'ул', 'house'=>'10Б', 'flat'=>nil, 'timezone'=>'UTC+4', 'normalized_address'=>{'source'=>'Саратов, ул.Электронная, д. 10б, к. 24', 'result'=>'г Саратов, ул Электронная, д 10Б к 24', 'postal_code'=>'410064', 'country'=>'Россия', 'region_fias_id'=>'df594e0e-a935-4664-9d26-0bae13f904fe', 'region_kladr_id'=>'6400000000000', 'region_with_type'=>'Саратовская обл', 'region_type'=>'обл', 'region_type_full'=>'область', 'region'=>'Саратовская', 'area_fias_id'=>nil, 'area_kladr_id'=>nil, 'area_with_type'=>nil, 'area_type'=>nil, 'area_type_full'=>nil, 'area'=>nil, 'city_fias_id'=>'bf465fda-7834-47d5-986b-ccdb584a85a6', 'city_kladr_id'=>'6400000100000', 'city_with_type'=>'г Саратов', 'city_type'=>'г', 'city_type_full'=>'город', 'city'=>'Саратов', 'city_area'=>nil, 'city_district_fias_id'=>nil, 'city_district_kladr_id'=>nil, 'city_district_with_type'=>'р-н Ленинский', 'city_district_type'=>'р-н', 'city_district_type_full'=>'район', 'city_district'=>'Ленинский', 'settlement_fias_id'=>nil, 'settlement_kladr_id'=>nil, 'settlement_with_type'=>nil, 'settlement_type'=>nil, 'settlement_type_full'=>nil, 'settlement'=>nil, 'street_fias_id'=>'82327ff0-396d-45ec-82d5-0e55370b6d72', 'street_kladr_id'=>'64000001000144100', 'street_with_type'=>'ул Электронная', 'street_type'=>'ул', 'street_type_full'=>'улица', 'street'=>'Электронная', 'house_fias_id'=>nil, 'house_kladr_id'=>nil, 'house_type'=>'д', 'house_type_full'=>'дом', 'house'=>'10Б', 'block_type'=>'к', 'block_type_full'=>'корпус', 'block'=>'24', 'flat_type'=>nil, 'flat_type_full'=>nil, 'flat'=>nil, 'flat_area'=>nil, 'square_meter_price'=>nil, 'flat_price'=>nil, 'postal_box'=>nil, 'fias_id'=>'82327ff0-396d-45ec-82d5-0e55370b6d72', 'fias_level'=>'7', 'kladr_id'=>'64000001000144100', 'capital_marker'=>'2', 'okato'=>'63401376000', 'oktmo'=>'63701000001', 'tax_office'=>'6453', 'tax_office_legal'=>'6453', 'timezone'=>'UTC+4', 'geo_lat'=>'51.6044736', 'geo_lon'=>'45.9823038', 'beltway_hit'=>nil, 'beltway_distance'=>nil, 'qc_geo'=>1, 'qc_complete'=>10, 'qc_house'=>10, 'qc'=>0, 'unparsed_parts'=>nil, 'metro'=>nil}, 'postcode'=>'410064'})
    end

    it 'returns a response with normalized address' do
      response = { :status=>:success, :body=>[normalized_address] }

      expect(model.normalize(address)).to eq(response)
    end
  end

  describe '#parse_status' do
    context 'if there is no passed params' do
      it 'returns incorrect status' do
        expect(model.parse_status(nil)).to eq(status: 'incorrect')
      end
    end

    context 'if status can not be parsed' do
      it 'returns nothing' do
        expect(model.parse_status('dadata_normalized_params_not_pochta_russia'=>true)).to eq(nil)
      end
    end

    context 'if normalized address can be used' do
      it 'returns correct status with empty tip' do
        expect(model.parse_status('validation-code'=>'VALIDATED', 'quality-code'=>'GOOD')).to eq(status: 'correct', error_tip: nil)
      end
    end

    context 'if normalized address can not be used' do
      it 'returns incorrect status with error tip' do
        error_tip = 'Код качества: UNDEF_01, Код проверки: NOT_VALIDATED_FOREIGN'
        expect(model.parse_status('validation-code'=>'NOT_VALIDATED_FOREIGN', 'quality-code'=>'UNDEF_01')).to eq(status: 'incorrect', error_tip: error_tip)
      end
    end
  end

  describe '#can_use?' do
    context 'when validation and quality codes is good' do
      it 'could be used' do
        expect(model.can_use?(normalized_address)).to be_truthy
      end
    end

    context 'when validation and quality codes is bad' do
      it 'could not be used' do
        poor_address = normalized_address
        poor_address['quality-code'] = 'UNDEF_01'

        expect(model.can_use?(poor_address)).to be_falsey
      end
    end
  end

  describe '#unclean?' do
    context 'if normalized address is unclean' do
      it 'returns true' do
        unclean_normalized_address = { 'validation-code'=>'NOT_VALIDATED_HAS_UNPARSED_PARTS' }
        expect(model.unclean?(unclean_normalized_address)).to be_truthy
      end
    end

    context 'if normalized address is not unclean' do
      it 'returns false' do
        clean_normalized_address = { 'validation-code'=>'VALIDATED' }
        expect(model.unclean?(clean_normalized_address)).to be_falsey
      end
    end
  end

  describe '#foreign?' do
    context 'if normalized address is foreign' do
      it 'returns true' do
        normalized_address = { 'validation-code'=>'NOT_VALIDATED_FOREIGN' }
        expect(model.foreign?(normalized_address)).to be_truthy
      end
    end

    context 'if normalized address is not foreign' do
      it 'returns false' do
        normalized_address = { 'validation-code'=>'VALIDATED' }
        expect(model.foreign?(normalized_address)).to be_falsey
      end
    end
  end
end
