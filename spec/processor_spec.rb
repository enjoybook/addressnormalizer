require 'spec_helper'

RSpec.describe AddressNormalizer::Processor do
  subject { model.normalize address }

  let(:model) { described_class.new }
  let(:address) { 'Саратов, ул.Электронная, д. 10б, к. 24' }
  let(:normalized_address) { { status: :success, body: [{'house'=>'10', 'region'=>'обл Саратовская', 'street'=>'ул Электронная', 'country'=>'Россия', 'timezone'=>'UTC+4', 'region_type'=>'обл', 'area_type'=>nil, 'city_type'=>'г', 'city_district_type'=>'р-н', 'settlement_type'=>nil, 'street_type'=>'ул', 'source'=>'Саратов, ул.Электронная, д. 10б, к. 24', 'postcode'=>'410064', 'flat'=>'24'}] } }
  let(:dadata_invalid_normalized_address) { { status: :success, body: [{ 'qc'=>'2', 'source'=>address }] } }
  let(:pochta_russia_invalid_normalized_address) { { status: :success, body: [{ 'validation-code'=>'NOT_VALIDATED' }] } }

  describe '#normalize' do
    let(:status_success) { { 'status'=>'success' } }

    context 'if the response from pochta russia has success status' do
      context 'and normalized address can be used' do
        it 'returns normalized address' do
          pochta_russia_double = instance_double AddressNormalizer::PochtaRussia, normalize: normalized_address, can_use?: true
          allow(AddressNormalizer::PochtaRussia).to receive(:new).and_return pochta_russia_double

          expect(subject).to eq(normalized_address[:body][0])
        end
      end

      context 'and normalized address can not be used' do
        context 'because of the address is foreign' do
          it 'returns incorrect status with a message about foreign country' do
            pochta_russia_double = instance_double AddressNormalizer::PochtaRussia, normalize: normalized_address, foreign?: true

            allow(pochta_russia_double).to receive(:can_use?).with(normalized_address[:body][0]).and_return false
            allow(AddressNormalizer::PochtaRussia).to receive(:new).and_return pochta_russia_double

            expect(subject).to eq(status: 'incorrect', error_tip: 'Иностранный адрес')
          end
        end

        context 'but it can be repaired' do
          context 'then try to repair with dadata' do
            before do
              allow(AddressNormalizer::PochtaRussia).to receive(:new).and_return pochta_russia_double
            end

            context 'and if the address was repaired' do
              let(:pochta_russia_double) { instance_double AddressNormalizer::PochtaRussia, normalize: normalized_address, unclean?: true, foreign?: false }

              it 'returns normalized address' do
                data_normalized_address = { status: :success, body: [{ 'qc'=>0 }] }
                allow(pochta_russia_double).to receive(:can_use?).and_return(false)

                dadata_double = instance_double AddressNormalizer::Dadata, normalize: data_normalized_address, can_use?: true, repair: nil
                allow(AddressNormalizer::Dadata).to receive(:new).and_return dadata_double

                expect(subject).to eq(data_normalized_address)
              end
            end

            context 'and if the address was not repaired' do
              context 'then use pochta russia again to normalize the address' do
                context 'if address was normalized successfully' do
                  let(:pochta_russia_double) { instance_double AddressNormalizer::PochtaRussia, normalize: normalized_address, unclean?: true, foreign?: false }

                  it 'returns the result' do
                    allow(pochta_russia_double).to receive(:can_use?).and_return(false)

                    dadata_double = instance_double AddressNormalizer::Dadata, normalize: dadata_invalid_normalized_address, can_use?: false, repair: dadata_invalid_normalized_address
                    allow(AddressNormalizer::Dadata).to receive(:new).and_return dadata_double

                    expect(subject).to eq(dadata_invalid_normalized_address)
                  end
                end

                context 'and if the address was not normalized again' do
                  let(:pochta_russia_double) { instance_double AddressNormalizer::PochtaRussia, normalize: pochta_russia_invalid_normalized_address, unclean?: true, foreign?: false }

                  it 'returns response from dadata as it is' do
                    allow(pochta_russia_double).to receive(:can_use?).and_return(false)

                    dadata_double = instance_double AddressNormalizer::Dadata, normalize: dadata_invalid_normalized_address, can_use?: false, repair: dadata_invalid_normalized_address
                    allow(AddressNormalizer::Dadata).to receive(:new).and_return dadata_double

                    expect(subject).to eq(dadata_invalid_normalized_address)
                  end
                end
              end
            end
          end
        end

        context 'and it can not be repaired' do
          it 'returns incorrect status with error message' do
            pochta_russia_double = instance_double AddressNormalizer::PochtaRussia, normalize: { status: :success, body: [{ source: address }] }, can_use?: false, foreign?: false, unclean?: false
            allow(AddressNormalizer::PochtaRussia).to receive(:new).and_return pochta_russia_double

            expect(subject).to eq(status: 'incorrect', error_tip: 'Невозможно разобрать адрес!')
          end
        end
      end
    end

    context 'if response from pochta russia has error status' do
      context 'then use dadata to normalize the address' do
        let(:pochta_russia_double) { instance_double AddressNormalizer::PochtaRussia, normalize: { status: 'incorrect' } }

        before do
          allow(AddressNormalizer::PochtaRussia).to receive(:new).and_return pochta_russia_double
          allow(AddressNormalizer::Dadata).to receive(:new).and_return dadata_double
        end

        context 'if the response from dadata has success status' do
          context 'and normalized address can be used' do
            let(:dadata_response) { { status: :success, body: { 'qc' => 0 } } }
            let(:dadata_double) { instance_double AddressNormalizer::Dadata, normalize: dadata_response, can_use?: true }

            it 'returns normalized address' do
              expect(subject).to eq(dadata_response)
            end
          end

          context 'and normalized address can not be used' do
            context 'but there is an opportunity to repair that address' do
              let(:dadata_response) { { status: :success, body: { 'qc' => 10 } } }
              let(:dadata_response_after_repair) { { status: :success, body: { 'qc' => 1 } } }
              let(:dadata_double) { instance_double AddressNormalizer::Dadata, normalize: dadata_response, can_use?: false, unclean?: true, clean: nil, repair: dadata_response_after_repair }

              it 'tries to repair it and returns the result as it is' do
                expect(subject).to eq(dadata_response_after_repair)
              end
            end

            context 'and the address can not be repaired' do
              let(:dadata_response) { { status: :success, body: { 'qc' => 10, unparsed_parts: nil } } }
              let(:dadata_response_after_repair) { { status: :success, body: { 'qc' => 10, unparsed_parts: nil } } }
              let(:dadata_double) { instance_double AddressNormalizer::Dadata, normalize: dadata_response, can_use?: false, unclean?: false, repair: dadata_response_after_repair }

              it 'returns result as it is' do
                expect(subject).to eq(dadata_response_after_repair)
              end
            end
          end
        end

        context 'if the response from dadata has error status' do
          let(:zero_balance_response) { { 'status'=>'incorrect', 'error_tip'=>'Zero balance!' } }
          let(:dadata_double) { instance_double AddressNormalizer::Dadata, normalize: zero_balance_response }

          it 'returns incorrect status with error message' do
            expect(subject).to eq(zero_balance_response)
          end
        end
      end
    end
  end

  describe '#parse_status' do
    context 'if status can be parsed with pochta_russia' do
      it 'returns status' do
        pochta_russia_params = { 'validation-code'=>'VALIDATED', 'quality-code'=>'GOOD' }
        expect(model.parse_status(pochta_russia_params)).to eq(status: 'correct', error_tip: nil)
      end
    end

    context 'if status can be parsed with dadata' do
      it 'returns status' do
        expect(model.parse_status('qc'=>0, 'qc_complete'=>10)).to eq(status: 'correct', error_tip: nil)
      end
    end

    context 'if status can not be parsed by any normalization services' do
      it 'returns err status with a message' do
        expect(model.parse_status({})).to eq(status: 'incorrect', error_tip: 'Невозможно определить статус!')
      end
    end
  end
end
