require 'spec_helper'

RSpec.describe AddressNormalizer::Configuration do
  subject { described_class.new }

  describe '#dadata_api_key=' do
    it 'sets dadata_api_key' do
      subject.dadata_api_key = 'api_key_test'
      expect(subject.dadata_api_key).to eq('api_key_test')
    end
  end
end
