require 'spec_helper'

RSpec.describe AddressNormalizer::Sender do
  describe '#request' do
    subject { described_class.new.request url, headers, data }

    let(:url) { 'http://example.com' }
    let(:headers) { { some: 'headers' } }
    let(:data) { { field: 'text' } }

    let(:connection_double) { instance_double Faraday::Connection }

    before do
      allow(Faraday).to receive(:new).with(url).and_return connection_double
    end

    it 'returns status with body of the response' do
      response_double = instance_double Faraday::Response, success?: true, body: ['body'].to_json

      # TODO: testing assignments of arguments in the block
      allow(connection_double).to receive(:post).and_return response_double

      expect(subject).to eq(status: :success, body: ['body'])
    end

    context 'when body response is wrong json' do
      it 'returns status with body of the response' do
        invalid_json = "{\"status\":\"ERROR\",\"message\":\"Token requests limit exc"
        response_double = instance_double Faraday::Response, success?: false, body: invalid_json

        allow(connection_double).to receive(:post).and_return response_double

        expect(subject).to eq(status: :error, body: invalid_json)
      end
    end

  end
end
